A research project on the artist contract with a series of workshops, presentations and screenings. Caveat takes as point of departure and arrival the contracts used in the art world as a tool for reconfiguring relations of authorship, labour and price among others.

## 2018

1.   The economy of the art system
2.   The economy of artistic practice
        +   On artistic labour
        +   On precarity
        +   On resilience
        +   On the commons
3.   The economy of the contract
        +   The legal pardigm
        +   The artist contract
        +   The contract as a document
        +   The artist as author(s)
        +   Contracting relationships
        +   Performing the contract

## 2019

1.   The ecology of the art system
2.   The ecology of the artistic practice
3.   The ecology of the contract